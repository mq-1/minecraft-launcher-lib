types
==========================
This module contains all Types for minecraft-launcher-lib. It may help your IDE. You don't need to use this module directly in your code.
If you are not interested in static typing just ignore it.
For more information about TypeDict see `PEP 589 <https://peps.python.org/pep-0589/>`_.

.. code:: python

{{types_source}}
