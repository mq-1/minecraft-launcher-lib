from .helper import download_file, parse_rule_list, inherit_json, empty, get_user_agent
from .natives import extract_natives_file, get_old_natives_string, get_natives_string
from typing import Any, Dict, Optional, Union
from .runtime import install_jvm_runtime
from .exceptions import VersionNotFound
from .types import CallbackDict
import requests
import shutil
import json
import os

__all__ = ["install_minecraft_version"]


def install_libraries(data: Dict[str, Any], path: str, callback: CallbackDict) -> None:
    """
    Install all libraries
    """
    session = requests.session()
    callback.get("setStatus", empty)("Download Libraries")
    callback.get("setMax", empty)(len(data["libraries"]) - 1)

    natives_string = get_natives_string()

    for i, library in enumerate(data["libraries"]):
        # Check, if the rules allow this lib for the current system
        if not parse_rule_list(library, "rules"):
            continue

        artifact = library["downloads"]["artifact"]
        artifact_path = os.path.join(path, "libraries", artifact["path"])

        # Check if the natives jar is for the current cpu architecture (for versions >= 1.19)
        if "natives" in artifact["path"]:
            if natives_string not in artifact["path"]:
                continue

        # Download the library
        download_file(artifact["url"], artifact_path, callback, sha1=artifact["sha1"], session=session)

        # Download the natives jar (for versions <= 1.18.2)
        if "classifiers" in library["downloads"]:
            natives_string = get_old_natives_string(library["downloads"]["classifiers"])
            artifact = library["downloads"]["classifiers"][natives_string]
            artifact_path = os.path.join(path, "libraries", artifact["path"])

            if natives_string in library["downloads"]["classifiers"]:
                download_file(artifact["url"], artifact_path, callback, sha1=artifact["sha1"], session=session)

            if "extract" in library:
                natives_dir = os.path.join(path, "versions", data["id"], "natives")
                extract_natives_file(artifact_path, natives_dir, library["extract"])


        # Increment download progress
        callback.get("setProgress", empty)(i)


def install_assets(data: Dict[str, Any], path: str, callback: CallbackDict) -> None:
    """
    Install all assets
    """
    # Old versions dosen't have this
    if "assetIndex" not in data:
        return
    callback.get("setStatus", empty)("Download Assets")
    session = requests.session()
    # Download all assets
    download_file(data["assetIndex"]["url"], os.path.join(path, "assets", "indexes", data["assets"] + ".json"), callback, sha1=data["assetIndex"]["sha1"], session=session)
    with open(os.path.join(path, "assets", "indexes", data["assets"] + ".json")) as f:
        assets_data = json.load(f)
    # The assets has a hash. e.g. c4dbabc820f04ba685694c63359429b22e3a62b5
    # With this hash, it can be download from https://resources.download.minecraft.net/c4/c4dbabc820f04ba685694c63359429b22e3a62b5
    # And saved at assets/objects/c4/c4dbabc820f04ba685694c63359429b22e3a62b5
    callback.get("setMax", empty)(len(assets_data["objects"]) - 1)
    count = 0
    for key, value in assets_data["objects"].items():
        download_file("https://resources.download.minecraft.net/" + value["hash"][:2] + "/" + value["hash"], os.path.join(path, "assets", "objects", value["hash"][:2], value["hash"]), callback, sha1=value["hash"], session=session)
        count += 1
        callback.get("setProgress", empty)(count)


def do_version_install(versionid: str, path: str, callback: CallbackDict, url: Optional[str] = None, sha1: Optional[str] = None) -> None:
    """
    Install the given version
    """
    # Download and read versions.json
    if url:
        download_file(url, os.path.join(path, "versions", versionid, versionid + ".json"), callback, sha1=sha1)
    with open(os.path.join(path, "versions", versionid, versionid + ".json")) as f:
        versiondata = json.load(f)
    # For Forge
    if "inheritsFrom" in versiondata:
        try:
            install_minecraft_version(versiondata["inheritsFrom"], path, callback=callback)
        except VersionNotFound:
            pass
        versiondata = inherit_json(versiondata, path)
    install_libraries(versiondata, path, callback)
    install_assets(versiondata, path, callback)
    # Download logging config
    if "logging" in versiondata:
        if len(versiondata["logging"]) != 0:
            logger_file = os.path.join(path, "assets", "log_configs", versiondata["logging"]["client"]["file"]["id"])
            download_file(versiondata["logging"]["client"]["file"]["url"], logger_file, callback, sha1=versiondata["logging"]["client"]["file"]["sha1"])
    # Download minecraft.jar
    if "downloads" in versiondata:
        download_file(versiondata["downloads"]["client"]["url"], os.path.join(path, "versions", versiondata["id"], versiondata["id"] + ".jar"), callback, sha1=versiondata["downloads"]["client"]["sha1"])
    # Need to copy jar for old forge versions
    if not os.path.isfile(os.path.join(path, "versions", versiondata["id"], versiondata["id"] + ".jar")) and "inheritsFrom" in versiondata:
        inheritsFrom = versiondata["inheritsFrom"]
        shutil.copyfile(os.path.join(path, "versions", versiondata["id"], versiondata["id"] + ".jar"), os.path.join(path, "versions", inheritsFrom, inheritsFrom + ".jar"))
    # Install java runtime if needed
    if "javaVersion" in versiondata:
        callback.get("setStatus", empty)("Install java runtime")
        install_jvm_runtime(versiondata["javaVersion"]["component"], path, callback=callback)
    callback.get("setStatus", empty)("Installation complete")


def install_minecraft_version(versionid: str, minecraft_directory: Union[str, os.PathLike], callback: Optional[CallbackDict] = None) -> None:
    """
    Install a Minecraft Version. Fore more Information take a look at the documentation"
    """
    if isinstance(minecraft_directory, os.PathLike):
        minecraft_directory = str(minecraft_directory)
    if callback is None:
        callback = {}
    if os.path.isfile(os.path.join(minecraft_directory, "versions", versionid, f"{versionid}.json")):
        do_version_install(versionid, minecraft_directory, callback)
        return
    version_list = requests.get("https://launchermeta.mojang.com/mc/game/version_manifest_v2.json", headers={"user-agent": get_user_agent()}).json()
    for i in version_list["versions"]:
        if i["id"] == versionid:
            do_version_install(versionid, minecraft_directory, callback, url=i["url"], sha1=i["sha1"])
            return
    raise VersionNotFound(versionid)
