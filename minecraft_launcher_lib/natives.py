from typing import Dict, Any, Optional
import platform
import zipfile
import os


def get_natives_string() -> Optional[str]:
    """
    Returns the natives string for the current system
    """
    match platform.system():
        case "Linux":
            return "natives-linux"
        case "Darwin":
            match platform.machine():
                case "x86_64":
                    return "natives-macos"
                case "arm64":
                    return "natives-macos-arm64"
        case "Windows":
            match platform.machine():
                case "AMD64":
                    return "natives-windows"
                case "x86":
                    return "natives-windows-x86"


def get_old_natives_string(classifiers: dict[str, Any]) -> Optional[str]:
    """
    Returns the old natives string for the current system
    """
    match platform.system():
        case "Linux":
            return "natives-linux"
        case "Darwin":
            if "natives-osx" in classifiers:
                return "natives-osx"
            return "natives-macos"
        case "Windows":
            return "natives-windows"


def extract_natives_file(filename: str, extract_path: str, extract_data: Dict[str, Any]) -> None:
    """
    Unpack natives
    """
    try:
        os.mkdir(extract_path)
    except Exception:
        pass
    zf = zipfile.ZipFile(filename, "r")
    for i in zf.namelist():
        for e in extract_data["exclude"]:
            if i.startswith(e):
                break
        else:
            zf.extract(i, extract_path)
